package com.hungerhaken.sqs;

public class SqsClientSideException extends SqsException {

	private static final long serialVersionUID = 5888117332086432633L;

	public SqsClientSideException(final String message) {
		super(message);
	}

}
