package com.hungerhaken.sqs.pojo;

import java.io.Serializable;

@SuppressWarnings({
    "PMD.CyclomaticComplexity",
    "PMD.ModifiedCyclomaticComplexity",
    "PMD.StdCyclomaticComplexity",
    "PMD.GodClass",
    "PMD.NPathComplexity",
    "PMD.UncommentedEmptyConstructor"})
public class IdentifiedReceiptHandle implements Serializable {

    private static final long serialVersionUID = -3640729009998780867L;

    private String id;

    private String receiptHandle;

    public IdentifiedReceiptHandle() {
    }

    public IdentifiedReceiptHandle(final String id, final String receiptHandle) {
        this.id = id;
        this.receiptHandle = receiptHandle;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getReceiptHandle() {
        return receiptHandle;
    }

    public void setReceiptHandle(final String receiptHandle) {
        this.receiptHandle = receiptHandle;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (id == null ? 0 : id.hashCode());
        result = prime * result + (receiptHandle == null ? 0 : receiptHandle.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IdentifiedReceiptHandle other = (IdentifiedReceiptHandle) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (receiptHandle == null) {
            if (other.receiptHandle != null) {
                return false;
            }
        } else if (!receiptHandle.equals(other.receiptHandle)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format("IdentifiedReceiptHandle [id=%s, receiptHandle=%s]", id, receiptHandle);
    }


}
