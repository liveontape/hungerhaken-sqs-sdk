package com.hungerhaken.sqs.pojo;

import java.io.Serializable;

@SuppressWarnings({ 
    "PMD.CyclomaticComplexity", 
    "PMD.ModifiedCyclomaticComplexity", 
    "PMD.StdCyclomaticComplexity",
    "PMD.GodClass", 
    "PMD.NPathComplexity",
    "PMD.UncommentedEmptyConstructor"})
public class HungerhakenClientHttpResponse implements Serializable {

    private static final long serialVersionUID = -5284184916912070408L;

    private int status;

    private String statusText;

    private String body;

    public HungerhakenClientHttpResponse() {
    }

    public HungerhakenClientHttpResponse(final int status, final String statusText, final String body) {
        this.status = status;
        this.statusText = statusText;
        this.body = body;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(final int status) {
        this.status = status;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(final String statusText) {
        this.statusText = statusText;
    }

    public String getBody() {
        return body;
    }

    public void setBody(final String body) {
        this.body = body;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (body == null ? 0 : body.hashCode());
        result = prime * result + status;
        result = prime * result + (statusText == null ? 0 : statusText.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HungerhakenClientHttpResponse other = (HungerhakenClientHttpResponse) obj;
        if (body == null) {
            if (other.body != null) {
                return false;
            }
        } else if (!body.equals(other.body)) {
            return false;
        }
        if (status != other.status) {
            return false;
        }
        if (statusText == null) {
            if (other.statusText != null) {
                return false;
            }
        } else if (!statusText.equals(other.statusText)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format("HungerhakenClientHttpResponse [status=%s, statusText=%s, body=%s]", status, statusText,
                body);
    }

}
