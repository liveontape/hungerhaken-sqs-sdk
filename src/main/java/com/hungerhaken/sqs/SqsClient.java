package com.hungerhaken.sqs;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.hungerhaken.sqs.pojo.HungerhakenClientHttpResponse;
import com.hungerhaken.sqs.pojo.IdentifiedReceiptHandle;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;

public final class SqsClient {

    private static final DateTimeFormatter AWS_SQS_EXPIRES_PARAM_DATE_FORMAT = DateTimeFormatter
            .ofPattern("yyyy-MM-dd'T'HH:mm:ssz").withZone(ZoneId.of("UTC"));

    private SqsClient() {
    }

    public static HungerhakenClientHttpResponse deleteMessage(final String queueUrl, final String receiptHandle)
            throws SqsException {
        try {
            final HttpResponse<String> httpResponse = Unirest.post(queueUrl).queryString("Action", "DeleteMessage")
                    .queryString("ReceiptHandle", receiptHandle).queryString("Version", "2012-11-05")
                    .queryString("Expires", inFourDays(AWS_SQS_EXPIRES_PARAM_DATE_FORMAT))
                    .header("Connection", "keep-alive").asString();
            checkHttpStatus(httpResponse.getStatus(), httpResponse.getStatusText());
            return new HungerhakenClientHttpResponse(httpResponse.getStatus(), httpResponse.getStatusText(),
                    httpResponse.getBody());
        } catch (final UnirestException e) {
            throw new SqsException(e.getMessage(), e);
        }
    }

    public static HungerhakenClientHttpResponse deleteMessageBatch(final String queueUrl,
            final List<IdentifiedReceiptHandle> receiptHandles) throws SqsException {
        try {
            final HttpRequestWithBody httpRequest = Unirest.post(queueUrl);
            int index = 1;
            for (final IdentifiedReceiptHandle entry : receiptHandles) {
                httpRequest.queryString("DeleteMessageBatchRequestEntry." + index + ".Id", entry.getId());
                httpRequest.queryString("DeleteMessageBatchRequestEntry." + index + ".Id", entry.getReceiptHandle());
                index++;
            }
            final HttpResponse<String> httpResponse = httpRequest.queryString("Action", "DeleteMessageBatch")
                    .queryString("Version", "2012-11-05")
                    .queryString("Expires", inFourDays(AWS_SQS_EXPIRES_PARAM_DATE_FORMAT))
                    .header("Connection", "keep-alive").asString();
            checkHttpStatus(httpResponse.getStatus(), httpResponse.getStatusText());
            return new HungerhakenClientHttpResponse(httpResponse.getStatus(), httpResponse.getStatusText(),
                    httpResponse.getBody());
        } catch (final UnirestException e) {
            throw new SqsException(e.getMessage(), e);
        }
    }

    private static void checkHttpStatus(final int status, final String statusText) throws SqsException {
        final String _status = String.valueOf(status);
        if (_status.charAt(0) == '2') {
            return;
        } else if (_status.charAt(0) == '4') {
            throw new SqsClientSideException(statusText);
        } else if (_status.charAt(0) == '5') {
            throw new SqsServerSideException(statusText);
        } else {
            throw new SqsException("Unknown error occured when trying to access queue URL");
        }
    }

    // "2020-04-18T22:52:43UTC"
    private static String inFourDays(final DateTimeFormatter dateTimeFormatter) {
        return OffsetDateTime.now().plusDays(4).format(dateTimeFormatter);
    }

}
