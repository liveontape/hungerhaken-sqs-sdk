package com.hungerhaken.sqs;

public class SqsServerSideException extends SqsException {

	private static final long serialVersionUID = 5888117332086432633L;

	public SqsServerSideException(final String message) {
		super(message);
	}

}
