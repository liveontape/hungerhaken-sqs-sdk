package com.hungerhaken.sqs;

public class SqsException extends Exception {

	private static final long serialVersionUID = -3202667835093677782L;

	public SqsException(final String message) {
		super(message);
	}

	public SqsException(final String message, final Throwable cause) {
		super(message, cause);
	}

}