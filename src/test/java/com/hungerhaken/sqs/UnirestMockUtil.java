package com.hungerhaken.sqs;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;

final class UnirestMockUtil {

	private UnirestMockUtil() {
	}

	static void mockUnirestPostRequest(final String url, final int responseStatusCode,
			final String responseStatusText, final String responseBody) throws UnirestException {
		final HttpRequestWithBody postRequest = PowerMockito.mock(HttpRequestWithBody.class);
		mockStatic(Unirest.class);
		when(Unirest.post(url)).thenReturn(postRequest);
		when(postRequest.header(Mockito.anyString(), Mockito.anyString())).thenReturn(postRequest);
		when(postRequest.queryString(Mockito.anyString(), Mockito.anyString())).thenReturn(postRequest);
		@SuppressWarnings("unchecked")
		final HttpResponse<String> httpResponse = mock(HttpResponse.class);
		when(httpResponse.getStatus()).thenReturn(responseStatusCode);
		when(httpResponse.getStatusText()).thenReturn(responseStatusText);
		when(httpResponse.getBody()).thenReturn(responseBody);
		when(postRequest.asString()).thenReturn(httpResponse);
	}

	static void mockUnirestPostRequestCausingException(final String url, final int responseStatusCode,
			final String responseStatusText, final String responseBody) throws UnirestException {
		final HttpRequestWithBody postRequest = PowerMockito.mock(HttpRequestWithBody.class);
		mockStatic(Unirest.class);
		when(Unirest.post(url)).thenReturn(postRequest);
		when(postRequest.header(Mockito.anyString(), Mockito.anyString())).thenReturn(postRequest);
		when(postRequest.queryString(Mockito.anyString(), Mockito.anyString())).thenReturn(postRequest);
		@SuppressWarnings("unchecked")
		final HttpResponse<String> httpResponse = mock(HttpResponse.class);
		when(httpResponse.getStatus()).thenReturn(responseStatusCode);
		when(httpResponse.getStatusText()).thenReturn(responseStatusText);
		when(httpResponse.getBody()).thenReturn(responseBody);
		when(postRequest.asString()).thenThrow(UnirestException.class);
	}
}
