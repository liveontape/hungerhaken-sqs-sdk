package com.hungerhaken.sqs;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.hungerhaken.sqs.pojo.HungerhakenClientHttpResponse;
import com.hungerhaken.sqs.pojo.IdentifiedReceiptHandle;
import com.mashape.unirest.http.Unirest;

import junit.framework.TestCase;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Unirest.class })
public class SqsClientTest {

	private static final int HTTP_OK = 200;
	private static final String RECEIPT_HANDLE = "1";
	private static final String QUEUE_URL = "https://localost/sqs/unittest";
	private static final List<IdentifiedReceiptHandle> IDENTIFIED_RECEIPT_HANDLES;
	static {
	    IDENTIFIED_RECEIPT_HANDLES = new ArrayList<>();
	    for (int index = 1; index <= 10; index++) {
	        IDENTIFIED_RECEIPT_HANDLES.add(new IdentifiedReceiptHandle("msg_" + index, index + ""));
	    }
	}

	@Test
	public void shouldExitWithoutExceptionIfSqsReturnsHttp200() throws Exception {
		PowerMockito.mockStatic(Unirest.class);
		UnirestMockUtil.mockUnirestPostRequest(QUEUE_URL, HTTP_OK, null, null);
		final HungerhakenClientHttpResponse res = SqsClient.deleteMessage(QUEUE_URL, RECEIPT_HANDLE);
		TestCase.assertEquals(200, res.getStatus());
		TestCase.assertNull(res.getStatusText());
		TestCase.assertNull(res.getBody());
	}

	@Test(expected = SqsClientSideException.class)
	public void shouldThrowSqsClientSideExceptionIfSqsReturnsHttp400AccessDenied() throws Exception {
		PowerMockito.mockStatic(Unirest.class);
		UnirestMockUtil.mockUnirestPostRequest(QUEUE_URL, 400, "AccessDeniedException", "You do not have sufficient access to perform this action.");
		SqsClient.deleteMessage(QUEUE_URL, RECEIPT_HANDLE);
	}

	@Test(expected = SqsServerSideException.class)
	public void shouldThrowSqsServerSideExceptionIfSqsReturnsHttp500InternalFailure() throws Exception {
		PowerMockito.mockStatic(Unirest.class);
		UnirestMockUtil.mockUnirestPostRequest(QUEUE_URL, 500, "InternalFailure", "The request processing has failed because of an unknown error, exception or failure.");
		SqsClient.deleteMessage(QUEUE_URL, RECEIPT_HANDLE);
	}

	@Test(expected = SqsException.class)
	public void shouldThrowSqsExceptionIfSqsReturnsNotAnticipatedHttpStatus() throws Exception {
		PowerMockito.mockStatic(Unirest.class);
		UnirestMockUtil.mockUnirestPostRequest(QUEUE_URL, 0, null, null);
		SqsClient.deleteMessage(QUEUE_URL, RECEIPT_HANDLE);
	}

	@Test(expected = SqsException.class)
	public void shouldThrowSqsExceptionIfUnirestThrowsAnUnirestException() throws Exception {
		PowerMockito.mockStatic(Unirest.class);
		UnirestMockUtil.mockUnirestPostRequestCausingException(QUEUE_URL, 0, null, null);
		SqsClient.deleteMessage(QUEUE_URL, RECEIPT_HANDLE);
	}

    @Test
    public void shouldExitWithoutExceptionIfSqsReturnsHttp200ForBatchedDeletion() throws Exception {
        PowerMockito.mockStatic(Unirest.class);
        UnirestMockUtil.mockUnirestPostRequest(QUEUE_URL, HTTP_OK, null, null);
        final HungerhakenClientHttpResponse res = SqsClient.deleteMessageBatch(QUEUE_URL, IDENTIFIED_RECEIPT_HANDLES);
        TestCase.assertEquals(200, res.getStatus());
        TestCase.assertNull(res.getStatusText());
        TestCase.assertNull(res.getBody());
    }

    @Test(expected = SqsClientSideException.class)
    public void shouldThrowSqsClientSideExceptionIfSqsReturnsHttp400AccessDeniedForBatchedDeletion() throws Exception {
        PowerMockito.mockStatic(Unirest.class);
        UnirestMockUtil.mockUnirestPostRequest(QUEUE_URL, 400, "AccessDeniedException", "You do not have sufficient access to perform this action.");
        SqsClient.deleteMessageBatch(QUEUE_URL, IDENTIFIED_RECEIPT_HANDLES);
    }

    @Test(expected = SqsServerSideException.class)
    public void shouldThrowSqsServerSideExceptionIfSqsReturnsHttp500InternalFailureForBatchedDeletion() throws Exception {
        PowerMockito.mockStatic(Unirest.class);
        UnirestMockUtil.mockUnirestPostRequest(QUEUE_URL, 500, "InternalFailure", "The request processing has failed because of an unknown error, exception or failure.");
        SqsClient.deleteMessageBatch(QUEUE_URL, IDENTIFIED_RECEIPT_HANDLES);
    }

    @Test(expected = SqsException.class)
    public void shouldThrowSqsExceptionIfSqsReturnsNotAnticipatedHttpStatusForBatchedDeletion() throws Exception {
        PowerMockito.mockStatic(Unirest.class);
        UnirestMockUtil.mockUnirestPostRequest(QUEUE_URL, 0, null, null);
        SqsClient.deleteMessageBatch(QUEUE_URL, IDENTIFIED_RECEIPT_HANDLES);
    }

    @Test(expected = SqsException.class)
    public void shouldThrowSqsExceptionIfUnirestThrowsAnUnirestExceptionForBatchedDeletion() throws Exception {
        PowerMockito.mockStatic(Unirest.class);
        UnirestMockUtil.mockUnirestPostRequestCausingException(QUEUE_URL, 0, null, null);
        SqsClient.deleteMessageBatch(QUEUE_URL, IDENTIFIED_RECEIPT_HANDLES);
    }

}
