# hungerhaken-sqs-sdk

## Background
While using the AWS Lambda Service with Java 8 based functions, we suffered from very long "cold start" times, up to 10 seconds even for pretty small functions.
Besides the inevitable delay caused by the VM start itself, we realized that creating the clients of the AWS SDK for Java was a very expensive and time consuming operation. 

To create an SQS client object from the AWS SDK for example took 2.5 seconds before it could be used. 
In a Lambda Service environment, where time and memory are a pricey and limited, this is really not cool. 

So we started to experiment with a much leaner approach to access AWS cloud resources by just using the base HTTP level API. 
And to make things a bit more handy, we created a very lean wrapper around the HTTP access so you don't have to deal with the HTTP stuff (URL encoding etc) directly. 
We called this wrapper "hungerhaken" SDK. "Hungerhaken" is german and can be translated loosely as "skin and bones", underlining the "as skinny as possible" concept of this approach.
To use the hungerhaken SDK, you don't need to create an object; all methods can be accessed in a static way; no need to create or preserve any state, so the hungerhaken SDK is threadsafe by nature, consumes less memory and is faster ready-to-use since no objects need to be created.
We believe that, for example, deleting an SQS message should not be more complicated than this:
```java
SqsDeleteClient.deleteMessage(yourQueueUrl, yourReceiptHandle);
```

And since we are releasing hungerhaken to [Maven Central](https://mvnrepository.com/artifact/com.hungerhaken/hungerhaken-sqs-sdk), 
you can just easily integrate the artifact into your Maven dependencies:
```gradle
compile group: 'com.hungerhaken', name: 'hungerhaken-sqs-sdk', version: '0.0.2'
```

And that's it!

The hungerhaken SDK is not meant to replace the entire AWS SDK. It is a purely pragmatically built artifact optimized for its usage in the AWS Lambda Service, supporting access to the most relevant AWS resources and features. 
And it is heavily work in progress; so far we only support SQS partially(!), and we still have no automatic integration test coverage which e.g. could be based on [localstack](https://github.com/localstack/localstack).

Our first tests with SQS.DeleteMessage looked very promising; the delete performance was the same as the original AWS SQS SDK, but without the lead time of 2.5 seconds to create the AWS SQS SDK client.
The advantage is very obvious: You save time and memory for the Lambda function execution, plus you need less code since you don't need to create a client object.
Makes sense, doesn't it?

>  Why don't you just go for Node.js based Lambda functions then without all this Java cold start crap?

Of course we use Node.js based Lambda functions as well, but there are still enough projects and codebase out there based on (or even restricted to) Java. 
Or you simply still prefer using Java - whatever your reason is to still use Java in Lambda service functions that access AWS resources - give hungerhaken a shot! 

If you like it, but your specific feature is not yet implemented (which is pretty likely since we just started this project), please feel free to contribute - any hand is needed and welcome! 
So join us and become a hungerhaken, too!

## How to release
To perform a release just execute these commands in the terminal from the project root.
```
git checkout master

git fetch

git reset --hard origin/master

./gradlew release -Prelease.useAutomaticVersion=true
```
A release job will be triggered on gitlab and then you can download the 'fatJar' artifact from gitlab release job